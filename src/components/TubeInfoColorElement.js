import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {textColor} from "../style/colors";

const TubeInfoColorElement = ({title, text}) => {
    const color = text ? '#'+text : '#FFFFFF00';
    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>{title}</Text>
            <View style={[{backgroundColor:color}, styles.color]}></View>
        </View>
    );
};
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 16,
    },
    text: {
        color: textColor,
        fontSize: 20,
    },
    color: {
        height: '100%',
        aspectRatio: 1,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#FFFFFF',
    },
})


export default TubeInfoColorElement;
