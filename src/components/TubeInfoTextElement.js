import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {textColor} from "../style/colors";

const TubeInfoTextElement = ({title, text}) => {
    useEffect(() => {
        return () => {
            console.log('Unmount screen 1');
        };
    }, []);

    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>{title}</Text>
            <Text style={styles.text}>{text}</Text>
        </View>
    );
};
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 16,
    },
    text: {
        color: textColor,
        fontSize: 20,
    }
})

export default TubeInfoTextElement;
