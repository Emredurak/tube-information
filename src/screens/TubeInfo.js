import React, {useEffect, useState} from 'react';
import {ScrollView, Text, View, StyleSheet, ActivityIndicator} from 'react-native';
import TubeInfoTextElement from "../components/TubeInfoTextElement";
import {appBackgroundColor, textColor} from "../style/colors";
import TubeInfoColorElement from "../components/TubeInfoColorElement";

const TubeInfo = () => {
    const [dataFetched, setDataFetched] = useState(false);
    const [data, setData] = useState(false);
    const apiURL = 'https://run.mocky.io/v3/4ffbefac-db13-4ce3-8050-82017c81afb3';
    const getData = async () => {
        try {
            const response = await fetch(apiURL);
            const json = await response.json();
            setDataFetched(true);
            setData(json);
        } catch (error) {
            console.error(error);
        } finally {
        }
    }

    useEffect(() => {
        getData();
        return () => {
            console.log('Unmount screen 1');
        };
    }, []);

    if (!dataFetched) {
        return (
            <ActivityIndicator
                style={styles.activityIndicator}
                size={'large'}
                color={'white'}
            />
        );
    }
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>Tube Information</Text>
            <TubeInfoTextElement title={'Code'} text={data.code}/>
            <TubeInfoTextElement title={'Manufacturer'} text={data.tubeManufacturer}/>
            <TubeInfoTextElement title={'Material'} text={data.material}/>
            <TubeInfoTextElement title={'Type'} text={data.type}/>
            <TubeInfoTextElement title={'Additive'} text={data.additive}/>
            <TubeInfoTextElement title={'Catalogue number'} text={data.catNumber}/>
            <TubeInfoColorElement title={'Cap color'} text={data.capColorHex} />
            <TubeInfoTextElement title={'Aspiration type'} text={data.aspirationType}/>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    activityIndicator: {
        alignItems: 'center',
        flex: 1,
    },
    container: {
        backgroundColor:appBackgroundColor,
        flex: 1,
        paddingHorizontal: 32,
    },
    header: {
        fontSize: 24,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: textColor,
        marginVertical: 24,
    },
})
export default TubeInfo;
