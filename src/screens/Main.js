import React from 'react';
import {Text, TouchableNativeFeedback, View, StyleSheet} from 'react-native';
import {appBackgroundColor, textColor} from "../style/colors";

const Main = ({navigation}) => {

  function goToTubeInfo() {
    navigation.replace('Tube Information');
  }

  return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: textColor}}>
        <TouchableNativeFeedback onPress={() => goToTubeInfo()}>
          <Text style={styles.button}>Tube Information</Text>
        </TouchableNativeFeedback>
      </View>
  );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: textColor,
        color: appBackgroundColor,
        paddingHorizontal: 16,
        paddingVertical: 12,
        textTransform: 'uppercase',
        borderRadius: 20,
        borderWidth: 1,
        borderColor: appBackgroundColor,
        elevation: 12,
    },
})
export default Main;
