import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {textColor} from './src/style/colors';
import Main from './src/screens/Main';
import TubeInfo from "./src/screens/TubeInfo";

const Stack = createStackNavigator();
function App() {
  let initialRoute = 'Main';
  return (
        <NavigationContainer
            style={{flex: '1'}}>
          <Stack.Navigator
              initialRouteName={initialRoute}
              screenOptions={{
                gestureEnabled: false,
                headerTitleAlign: 'center',
                headerTitleStyle: {
                  color: textColor,
                },
              }}>
            <Stack.Screen name="Main" component={Main} options={{headerShown: false}}/>
            <Stack.Screen name="Tube Information" component={TubeInfo} options={{headerShown: false}}/>
          </Stack.Navigator>
        </NavigationContainer>
  );
}

export default App;
